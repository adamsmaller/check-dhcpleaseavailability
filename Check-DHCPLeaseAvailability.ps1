# Check if the DHCP Server role is installed on the server
if ( !(Get-WindowsFeature DHCP | where {$_.InstallState -eq 'Installed'}) ) {
    Write-Host "DHCP Server not installed."
    Exit 0
}

$recent = (Get-Date).AddHours(-1)
$now = Get-Date -Format "[dd/MM/yyyy HH:MM:ss]"
$freeLeases = Get-DhcpServerv4ScopeStatistics

# Searches Event Viewer for logs matching event ID '1020' that have been created in the past hour.
$leaseError = Get-WinEvent -MaxEvents 10 -ProviderName "Microsoft-Windows-DHCP-Server" | where {$_.Id -eq 1020 -and $_.TimeCreated -gt $recent}

if ($leaseError -and $freeLeases.Free -lt 5) {
    Write-Host "$now - There are currently $($freeLeases.Free) leases available.`n`n$f[$($leaseError[0].TimeCreated)] - $($leaseError[0].Message)"
    Exit 1
}

elseif ($freeLeases.Free -gt 5) {
    Write-Host "Sufficient unallocated leases available."
    Exit 0
}

<#
$subnet = Get-NetIPAddress | where {$_.AddressFamily -eq "IPv4" -and $_.IPAddress -ne "127.0.0.1"}
$subnet = "$($subnet.IPAddress.Split(".")[0..2] -Join '.').0"

$scope = Get-DhcpServerv4ScopeStatistics | where {$_.ScopeId -eq $subnet}
#>
