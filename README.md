# Check-DHCPLeaseAvailability


## Purpose

I created this script to have the address availability of DHCP pools monitored so if lease availability became low, the helpdesk could proactively increase the pool range or tidy up assigned leases.
